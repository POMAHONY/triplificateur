import sys
import os

def line_separator(string) :
    '''sépare les lignes du fichier csv'''
    return string.split('\n')[:-1]

def column_separator(string, sep) :
    '''sépare les colonne d'une ligne du fichier csv'''
    return string.split(sep)


def triplificateur(file, sep, result_file) :
    '''fonction principale du triplificateur'''
    f = open(file, "r+") #on ouvre le fichier csv
    string = f.read() #on lit son contenu et on le stocke dans une chaiine de caractère
    lines = line_separator(string) #on stocke toutes les lignes du fichiers dans une liste
    res, prefix_objet, prefix_sujet, prefix_predicat = [], " obj:", "su:", " pred:"
    for line in lines : #pour chaque ligne on sépare les valeurs des colonnes dans une liste
        column = column_separator(line, sep)
        res.append(column)
    column_t = res[0]
    if os.path.exists(result_file):
        os.remove(result_file)
    f1 = open(result_file, "a")
    for k in range(1, len(lines)) : #on écrit dans un fichier .ttl l'équivalent en turtle de tous les triplets que l'on extrait du fichier csv
        for i in range(len(column_t)) :
            if(res[k][i] != '') :
                if (i==0 and i ==len(column_t)) :
                    string = prefix_sujet+"Ligne_" + str(k) + prefix_predicat+column_t[i] + prefix_objet+res[k][i] + ' .' + '\n'
                    f1.write(string)
                elif i == 0 :
                    string = prefix_sujet+"Ligne_" + str(k) + prefix_predicat+column_t[i] + prefix_objet+res[k][i]+ ' ;' + '\n'
                    f1.write(string)
                elif i == len(column_t) -1 :
                    string = "          " + prefix_predicat+column[i] + prefix_objet+res[k][i] + ' .' + '\n'
                    f1.write(string)
                else :
                    string = "          " + prefix_predicat+column_t[i] + prefix_objet+res[k][i] + ' ;' + '\n'
                    f1.write(string)
    f1.close()

if __name__ =='__main__' :
    triplificateur(sys.argv[1], sys.argv[3], sys.argv[2])
